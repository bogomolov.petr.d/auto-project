package petr_bogomolov.com.auto.project.car;

import petr_bogomolov.com.auto.project.car.elements.properties_of_elements.ColorWheels;
import petr_bogomolov.com.auto.project.car.elements.properties_of_elements.Drive;
import petr_bogomolov.com.auto.project.car.elements.properties_of_elements.TypeOfMotor;
import petr_bogomolov.com.auto.project.car.elements.properties_of_elements.TypeOfTransmission;
import petr_bogomolov.com.auto.project.car.properties_of_car.BodyType;
import petr_bogomolov.com.auto.project.car.properties_of_car.Brakes;
import petr_bogomolov.com.auto.project.car.elements.Motor;
import petr_bogomolov.com.auto.project.car.elements.Transmission;
import petr_bogomolov.com.auto.project.car.elements.Wheels;

public class Car {

    Motor motor = new Motor(TypeOfMotor.BENZIN,200, 2.2);
    Transmission transmission = new Transmission(TypeOfTransmission.AUTO, 6, Drive.FRONT);
    Wheels wheels = new Wheels(ColorWheels.BLACK);

    private String nameBrand;
    private String nameModel;
    private BodyType bodyType;
    private Motor motor1;
    private Transmission transmission1;
    private Brakes brakes;
    private Wheels wheels1;
    private int maxSpeed;
    private double accelerationFrom_0_before_100;
    private int weight;
    private int maxWeight;

    public Car(String nameBrand, String nameModel, BodyType bodyType, Motor motor1,
               Transmission transmission1, int maxSpeed, double accelerationFrom_0_before_100,
               int weight, int maxWeight) {

        this.nameBrand = nameBrand;
        this.nameModel = nameModel;
        this.bodyType = bodyType;
        this.motor1 = motor1;
        this.transmission1 = transmission1;
        this.maxSpeed = maxSpeed;
        this.accelerationFrom_0_before_100 = accelerationFrom_0_before_100;
        this.weight = weight;
        this.maxWeight = maxWeight;
    }

    @Override
    public String toString() {
        return "Name Brand = " + nameBrand +
                " nameModel=" + nameModel +
                " bodyType=" + bodyType +
                " type of motor " + motor +
                " transmission=" + transmission +
                " brakes=" + brakes +
                " wheels=" + wheels +
                " maxSpeed=" + maxSpeed +
                " acceleration 0 - 100 km - " + accelerationFrom_0_before_100 +
                " weight=" + weight + " kg " +
                " maxWeight - " + maxWeight + " kg ";
    }
}
