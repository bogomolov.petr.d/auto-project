package petr_bogomolov.com.auto.project.car.elements;

import petr_bogomolov.com.auto.project.car.elements.properties_of_elements.TypeOfMotor;

public class Motor {

    private TypeOfMotor typeOfMotor;
    private int power;
    private double volumMotor;

    public Motor(TypeOfMotor typeOfMotor, int power, double volumMotor) {
        this.typeOfMotor = typeOfMotor;
        this.power = power;
        this.volumMotor = volumMotor;
    }

    @Override
    public String toString() {
        return  typeOfMotor +
                ", power=" + power +
                ", volumMotor=" + volumMotor +
                '}';
    }
}
