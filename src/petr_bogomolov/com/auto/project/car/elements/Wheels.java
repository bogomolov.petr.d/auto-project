package petr_bogomolov.com.auto.project.car.elements;

import petr_bogomolov.com.auto.project.car.elements.properties_of_elements.ColorWheels;

public class Wheels {

    private ColorWheels colorWheels;

    public Wheels(ColorWheels colorWheels) {
        this.colorWheels = colorWheels;
    }

    @Override
    public String toString() {
        return "Wheels{" +
                "colorWheels=" + colorWheels +
                '}';
    }
}
