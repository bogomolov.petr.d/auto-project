package petr_bogomolov.com.auto.project.car.elements;

import petr_bogomolov.com.auto.project.car.elements.properties_of_elements.Drive;
import petr_bogomolov.com.auto.project.car.elements.properties_of_elements.TypeOfTransmission;

public class Transmission {

    private TypeOfTransmission typeOfTransmission;
    private int numberOfStages;
    private Drive drive;

    public Transmission(TypeOfTransmission typeOfTransmission, int numberOfStages, Drive drive) {
        this.typeOfTransmission = typeOfTransmission;
        this.numberOfStages = numberOfStages;
        this.drive = drive;
    }

    @Override
    public String toString() {
        return "Transmission{" +
                "typeOfTransmission=" + typeOfTransmission +
                ", numberOfStages=" + numberOfStages +
                ", drive=" + drive +
                '}';
    }
}
