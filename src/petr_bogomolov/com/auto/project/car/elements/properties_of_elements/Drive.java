package petr_bogomolov.com.auto.project.car.elements.properties_of_elements;

public enum Drive {
    FRONT,
    BACK,
    FULL
}
