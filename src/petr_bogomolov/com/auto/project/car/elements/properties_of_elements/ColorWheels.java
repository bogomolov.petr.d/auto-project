package petr_bogomolov.com.auto.project.car.elements.properties_of_elements;

public enum ColorWheels {
    BLACK,
    SILVER,
    RED,
    WHITE
}
