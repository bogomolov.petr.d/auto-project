package petr_bogomolov.com.auto.project.car.properties_of_car;

public enum Brakes {
    CERAMIC,
    METAL
}
