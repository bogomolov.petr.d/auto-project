package petr_bogomolov.com.auto.project.car.properties_of_car;

public enum BodyType {
    SEDAN,
    MINIVAN,
    JEEP,
    CABRIOLET,
    HATCHBACK
}
