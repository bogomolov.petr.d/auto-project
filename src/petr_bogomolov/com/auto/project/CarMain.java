package petr_bogomolov.com.auto.project;

import petr_bogomolov.com.auto.project.car.Car;
import petr_bogomolov.com.auto.project.car.elements.Motor;
import petr_bogomolov.com.auto.project.car.elements.Transmission;
import petr_bogomolov.com.auto.project.car.elements.properties_of_elements.Drive;
import petr_bogomolov.com.auto.project.car.elements.properties_of_elements.TypeOfMotor;
import petr_bogomolov.com.auto.project.car.elements.properties_of_elements.TypeOfTransmission;
import petr_bogomolov.com.auto.project.car.properties_of_car.BodyType;

public class CarMain {
    public static void main(String[] args) {

        Motor motor = new Motor(TypeOfMotor.BENZIN,200, 2.2);
        Transmission transmission = new Transmission(TypeOfTransmission.AUTO, 6, Drive.FRONT);
        Car toyota = new Car("Toyota", "Auris", BodyType.HATCHBACK, motor,
                transmission, 250, 7.6, 1800, 2200 );

        System.out.println( toyota);
    }
}
